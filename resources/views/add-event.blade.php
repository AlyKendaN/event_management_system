@extends('maindashboard')
@section('page-title','Add Event')
@section('page-main-breadcrumb','Add Event')
@section('page-parent-breadcrumb','Home')
@section('page-child-breadcrumb','Add Event')

@section('page-content')

<div class="row">
                    <div class="col-12">

                    <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Add New Event</h4>
                                <form action="#" class="validation-wizard wizard-circle">
                                    <!-- Step 1 -->
                                    <h6><strong>Event Info.</strong></h6>
                                    <section>

                                    <div class="row">
                                    <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="fname"> Event Name :  </label>
                                                    <input type="text" class="form-control required" id="ename" name="ename" required> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slug"> Slug : </label>
                                                    <input type="text" class="form-control required" id="slug" name="slug" required> </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="pic"> Event Image :  </label>
                                                    <input class="form-control required" type="file" name="pic" id="pic" accept="image/*">  
                                                                                          </div>  
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="date"> Start Date :  </label>
                                                    <input type="date" class="form-control required" id="sdate" name="sdate">

                                                    </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">

                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="edate"> End Date :  </label>
                                                    <input type="date" class="form-control required" id="edate" name="edate">

                                                    </div>
                                            </div>
                                            
                                            
                                        </div>

                                        

                                        
                                       <!-- <div class="row">
                                        
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="days">Period :</label>
                                                    <select class="custom-select form-control required" id="days" name="days" type="number">
                                                        <option value="">Event Days</option>
                                                        <option value="one">1</option>
                                                        <option value="two">2</option>
                                                        <option value="three">3</option>
                                                        <option value="four">4</option>
                                                        <option value="five">5</option>
                                                        <option value="six">6</option>
                                                        <option value="seven">7</option>
                                                        <option value="eight">8</option>
                                                        <option value="nine">9</option>
                                                        <option value="ten">10</option>
                                                        
                                                    </select></div></div>

                                                

                                            </div>-->
                                        
                                        
                                    </section>
                                    <!-- Step 2 -->
                                    <h6><strong>place</strong></h6>
                                    <section>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="eplace">Event Place :</label>
                                                    <input type="text" class="form-control required" id="eplace" name="eplace">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="eadress">Event Adress :</label>
                                                    <input type="text" class="form-control required" id="eadress" name="eadress"> </div>
                                            </div>
                                            
                                        </div>
                                    </section>
                                    <!-- Step 3 -->
                                    <h6><strong>Guests Numner</strong></h6>
                                    <section>
                                    <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="nguests">Expected Guests :</label>
                                                    <input type="number" class="form-control required" id="nguests" name="nguests">
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </section>
                                    <!-- Step 4 -->
                                    <h6><strong>Additional Information</strong></h6>
                                    <section>
                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                    <label for="edesc"> Description :</label>
                                                    <textarea name="edesc" id="edesc" rows="6" class="form-control"></textarea>
                                                    </div>
                                            </div>
                                           
                                            
                                            
                                        </div>



                                        <div class="row">

                                        <div class="col-md-6">
                                                <div class="form-group">
                                                <button type="submit" value="add" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Add Event</button>

                                                    </div>
                                            </div>
                                        </div>
                                    </section>
                                </form>
                            </div>
                        </div>



</div>
</div>


@endsection

@section('page-scripts')
<script src="{{asset("assets/plugins/styleswitcher/jQuery.style.switcher.js")}}"></script>

@endsection