@extends('maindashboard')
@section('page-title','Contact Us')
@section('page-main-breadcrumb','Contact Us')
@section('page-parent-breadcrumb','Home')
@section('page-child-breadcrumb','Contact US')

@section('page-content')
<div class="col-12">
                                    <div class="card-body">
                                        <h3 class="card-title">Get In Touch</h3>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="To:" name="to" id="to" required>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Subject:" name="subject" id="subject" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="textarea_editor form-control" rows="7" placeholder="Enter text ..." name="body" id="body" required></textarea>
                                        </div>
                                        
                                        
                                        <button type="submit" value="send" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection