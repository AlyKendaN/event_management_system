@extends('maindashboard')
@section('page-title','Edit Account')
@section('page-main-breadcrumb','Edit Account')
@section('page-parent-breadcrumb','Home')
@section('page-child-breadcrumb','Edit Account')

@section('page-content')

<div class="col-12">
                                    <div class="card-body">
                                        <h3 class="card-title">Edit Your Account</h3>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Name:" name="name,e" id="name" type="text" required>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Email:" name="email" id="email" type="email" required>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password:" name="pass" id="pass" type="password" required>
                                        </div>

                                        <div class="form-group">
                                        <input class="form-control" placeholder="Adress:" name="adress" id="adress" type="text" required>
                                        
                                        </div>
                                        <div class="form-group">
                                        <input class="form-control" placeholder="Phone Number:" name="phone" id="phone" type="tel" required>
                                        
                                        </div>


                                        
                                        
                                        <button type="submit" value="update" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection