@extends('maindashboard')
@section('page-title','Email')
@section('page-main-breadcrumb','Email')
@section('page-parent-breadcrumb','Home')
@section('page-child-breadcrumb','Email')

@section('page-content')

<div class="col-12">
                                    <div class="card-body">
                                        <h3 class="card-title">Compose New Message</h3>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Name:" name="name,e" id="name" type="text" required>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Email:" name="email" id="email" type="email" required>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Subject:" name="subject" id="subject" type="text" required>
                                        </div>

                                        <div class="form-group">
                                            <textarea class="textarea_editor form-control" rows="7" placeholder="Message:" name="msg" id="msg" type="text" required></textarea>
                                        </div>
                                        <div class="form-group">
                                        <input class="form-control required" type="file" name="pic" id="pic" accept="image/*">  

                                        </div>
                                        
                                        <button type="submit" value="send" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection