<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home#');
});
Route::get('/home', function () {
    return view('home#');
});
Route::get('/contact', function () {
    return view('contact-us');
});
Route::get('/edit', function () {
    return view('edit-acc');
});
Route::get('/messeges', function () {
    return view('messages');
});
Route::get('/email', function () {
    return view('email');
});
Route::get('/add', function () {
    return view('add-event');
});
Route::get('/about', function () {
    return view('about-us');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
